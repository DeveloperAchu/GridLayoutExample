package com.titans.gridlayoutexample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterImage extends RecyclerView.Adapter<AdapterImage.ViewHolder> {

    private List<Image> ImageList;
    private Context context;
    private LayoutInflater inflater;
    private ClickListener clickListener;

    public AdapterImage(ArrayList<Image> ImageList, Context context) {
        this.ImageList = ImageList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView LoadedImage;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            LoadedImage = (ImageView) v.findViewById(R.id.the_image);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.itemClicked(view, getLayoutPosition());
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View imageView = inflater.inflate(R.layout.url_image, parent, false);
        return new ViewHolder(imageView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Image image = ImageList.get(position);

        Glide.with(context)
                .load(image.getImageURL())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.LoadedImage);

    }

    @Override
    public int getItemCount() {
        return ImageList.size();
    }

    public interface ClickListener {
        void itemClicked(View v, int position);
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }
}
