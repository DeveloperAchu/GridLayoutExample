package com.titans.gridlayoutexample;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import java.io.Serializable;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private boolean initial = true;
    private boolean tabletUI = false;
    private List<Image> ImagesAfterRotate;
    private int position;
    private boolean listFragment = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        tabletUI = checkDevice();
    }

    @Override
    protected void onResume() {
        super.onResume();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (initial && tabletUI)
            fragmentTransaction.replace(R.id.tablet_image_detail, new ActivityImageDetail());

        if (tabletUI) {
            fragmentTransaction.replace(R.id.tablet_image_list, new ActivityImageList());
        } else {
            if(listFragment)
                fragmentTransaction.replace(R.id.phone_image_list, new ActivityImageList());
            else
                fragmentTransaction.replace(R.id.phone_image_list,new ActivityImageDetail());
        }

        fragmentTransaction.commit();
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("initial", initial);
        outState.putSerializable("personsAfterRotate", (Serializable) ImagesAfterRotate);
        outState.putBoolean("tabletUI", tabletUI);
        outState.putInt("position", position);
        outState.putBoolean("listFragment", listFragment);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        initial = savedInstanceState.getBoolean("initial");
        ImagesAfterRotate = (List<Image>) savedInstanceState.getSerializable("personsAfterRotate");
        tabletUI = savedInstanceState.getBoolean("tabletUI");
        position = savedInstanceState.getInt("position");
        listFragment = savedInstanceState.getBoolean("listFragment");
    }

    @Override
    public void onBackPressed() {
        if (!listFragment && !tabletUI) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.phone_image_list, new ActivityImageList());
            fragmentTransaction.commit();
            return;
        }
        super.onBackPressed();
    }


    private boolean checkDevice() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.tabletUI);

        if (linearLayout == null)
            return false;

        return true;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    public boolean isInitial() {
        return initial;
    }

    public List<Image> getImagesAfterRotate() {
        return ImagesAfterRotate;
    }

    public void setImagesAfterRotate(List<Image> ImagesAfterRotate) {
        this.ImagesAfterRotate = ImagesAfterRotate;
    }

    public boolean isTabletUI() {
        return tabletUI;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setListFragment(boolean listFragment) {
        this.listFragment = listFragment;
    }
}
