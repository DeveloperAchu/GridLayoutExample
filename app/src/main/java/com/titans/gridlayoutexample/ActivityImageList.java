package com.titans.gridlayoutexample;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ActivityImageList extends Fragment implements AdapterImage.ClickListener {

    ArrayList<Image> imageArrayList;
    RecyclerView recycleImages;
    boolean tabletUI;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.grid_image_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity)getActivity()).setListFragment(true);
        tabletUI = ((MainActivity) getActivity()).isTabletUI();
        recycleImages = (RecyclerView) getActivity().findViewById(R.id.image_list);

        if (((MainActivity) getActivity()).isInitial()) {
            imageArrayList = Image.getImageList(getContext());
            ((MainActivity) getActivity()).setImagesAfterRotate(imageArrayList);
            ((MainActivity) getActivity()).setInitial(false);
        } else {
            imageArrayList = (ArrayList<Image>) ((MainActivity) getActivity()).getImagesAfterRotate();
        }

        AdapterImage adapter = new AdapterImage(imageArrayList, getContext());
        adapter.setClickListener(this);
        recycleImages.setAdapter(adapter);

        int columns = getColumn();

        recycleImages.setLayoutManager(new GridLayoutManager(getContext(), columns));
    }

    private int getColumn() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        if (tabletUI)
            width = (int) (width * 0.25);

        float density = displayMetrics.density;
        float dp = width / density;

        return ((int) (dp / 100));
    }


    @Override
    public void itemClicked(View v, int position) {
        ((MainActivity) getActivity()).setPosition(position);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        if (tabletUI)
            fragmentTransaction.replace(R.id.tablet_image_detail, new ActivityImageDetail());

        else
            fragmentTransaction.replace(R.id.phone_image_list, new ActivityImageDetail());

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
