package com.titans.gridlayoutexample;

import android.content.Context;

import java.util.ArrayList;

public class Image {

    private String imageURL;

    public Image(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public static ArrayList<Image> getImageList(Context mContext) throws ArrayIndexOutOfBoundsException {
        String[] urls = mContext.getResources().getStringArray(R.array.image_urls);

        ArrayList<Image> images = new ArrayList<>();

        for (int i = 0; i < urls.length; i++) {
            images.add(
                    new Image(urls[i])
            );
        }
        return images;
    }
}
