package com.titans.gridlayoutexample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ActivityImageDetail extends Fragment {

    ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.image_detail, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            int position = ((MainActivity) getActivity()).getPosition();
            String[] urls = this.getResources().getStringArray(R.array.image_urls);
            imageView = (ImageView) getActivity().findViewById(R.id.image_detail);

            boolean initial = ((MainActivity) getActivity()).isInitial();

            if (initial) {
                Glide.with(this)
                        .load(urls[0])
                        .placeholder(R.mipmap.ic_launcher)
                        .into(imageView);
            } else {
                Glide.with(this)
                        .load(urls[position])
                        .placeholder(R.mipmap.ic_launcher)
                        .into(imageView);
            }
            ((MainActivity)getActivity()).setListFragment(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
